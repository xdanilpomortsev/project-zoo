//
// Created by avesi on 12/28/2020.
//

#ifndef PROJEKT_WATER_H
#define PROJEKT_WATER_H
#include "Potion.h"

class Water :public Potion{
public:
    Water();
    void printInfo();
};


#endif //PROJEKT_WATER_H
