//
// Created by 79923 on 14.12.2020.
//

#include "Hero.h"

Hero::Hero(std::string name, float health, float atack, float protection){
    m_inventory = new Inventory();
    m_name = name;
    m_health = health;
    m_atack = atack;
    m_weapon = new Weapon("knife", 30);
    m_armor = new Armor(30, "Gucci");
    m_protection = protection;

}
float Hero::getAtack(){
    return m_atack;
}
float Hero::getHelth(){
    return m_health;
}
float Hero::getProtection(){
    return m_protection;
}
void Hero::usePotion(Potion* potion){
    m_health = m_health + potion->getBonusHealth();
    std::cout << "You drinked " << potion->getType() << std::endl;
    std::cout << "Your health: " << getHelth() << std::endl;
}

void Hero::setArmor(Armor* armor){
    m_armor = armor;
}
void Hero::setWeapon(Weapon* weapon){
    m_weapon = weapon;
}
Inventory* Hero::getInventory(){
    return m_inventory;
}
void Hero::printInfo(){
    std::cout << "============Hero============\n";
    std::cout << "Name: " << m_name << "\n";
    std::cout << "Health: " << m_health << "\n";
    std::cout << "Atack: " << m_atack << "\n";
    std::cout << "Protection: " << m_protection << "\n";
    std::cout << "============Armor============\n ";
    m_armor->printInfo();
    std::cout << "============Armor============\n";
    m_weapon->printInfo();
}
std::string Hero::getName(){
    return m_name;
}
void Hero::atack(Enemy* enemy){
    if(enemy == nullptr){
        std::cout << "There are not enemy.\n";
    }
    if(enemy->getHelth()<1){
        std::cout << "Enemy is dead. U can't atack him.\n";
        return;
    }
    float g;
    if(getAtack() > enemy->getResist()) {
        g = enemy->getResist() + enemy->getHelth() - getAtack();
        if(g > 0)
        enemy->setHelth(g);
        else
            enemy->setHelth(0);
        std::cout << "You atacked enemy, enemy recieved " << getAtack()-enemy->getResist() << "points of damage. " << "Enemy has [" << enemy->getHelth() << "] hitpoints.\n";
        if(enemy->getAtack()>getProtection()){
            g = getProtection() + getHelth() - enemy->getAtack();
            if(g > 0){
                setHealth(g);
                std::cout << "Enemy atacked you. You recieved " << getAtack()-enemy->getResist() << "points of damage. " << "You have [" << getHelth() << "] hitpoints.\n";
            }
            else{
                std::cout << "You are dead. Game over.\n";
            }
        }else
            std::cout << "Enemy has low atack, you didn't recieve any damage\n";
    }
    else std::cout << "You have low atack, enemy didn't recieve any damage\n";
}
void Hero::setHealth(float health){
    m_health = health;
}
Weapon* Hero::getWeapon(){
    return m_weapon;
}
Armor* Hero::getArmor(){
    return m_armor;
}
void Hero::takeObject(Enemy* enemy){
    if(enemy->getObject()->getType()=="armor" || enemy->getObject()->getType()=="platinumarmor" || enemy->getObject()->getType()=="goldarmor")
        m_inventory->addArmor((Armor*)enemy->getObject());
    else if(enemy->getObject()->getType()=="potion" || enemy->getObject()->getType()=="kofola" || enemy->getObject()->getType()=="kozel" || enemy->getObject()->getType()=="water")
        m_inventory->addPotion((Potion*)enemy->getObject());
    else if(enemy->getObject()->getType()=="weapon" || enemy->getObject()->getType()=="weapon" || enemy->getObject()->getType()=="weapon")
        m_inventory->addWeapon((Weapon*)enemy->getObject());
    std::cout << "You have recieved : " << std::endl;
    enemy->getObject()->printInfo();
}