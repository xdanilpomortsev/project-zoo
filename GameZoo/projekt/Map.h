//
// Created by 79923 on 14.12.2020.
//

#ifndef PROJEKT_MAP_H
#define PROJEKT_MAP_H
#include <iostream>
#include "Enemy.h"
#include "Object.h"
#include "Hero.h"
#include <vector>
#include "Tile.h"
#include <cstdlib>

class Map {
private:
    std::vector<std::vector<Tile*>> m_map;
    void createMap(int sizeOne, int sizeTwo);
    int  m_TilePos[2];
    Enemy* m_CurrentEnemy;
    void setTile(int a, int b);
    Tile* m_Tile;
    void TileUp();
    void TileDown();
    void TileLeft();
    void TileRight();
    Hero* m_Hero;
    //Enemy* getEnemy();

public:
    Map(int sizeOne, int sizeTwo);
    void printMap();
    void goUp();
    void goDown();
    void goLeft();
    void goRight();
    Hero* getHero();
    Enemy* getEnemy();


};

#endif //PROJEKT_MAP_H
