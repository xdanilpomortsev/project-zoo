//
// Created by avesi on 12/24/2020.
//

#ifndef PROJEKT_GAMEBUILDER_H
#define PROJEKT_GAMEBUILDER_H


class GameBuilder {
private:
public:
    GameBuilder()= default;;
    void start();
};


#endif //PROJEKT_GAMEBUILDER_H
