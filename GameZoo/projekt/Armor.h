//
// Created by 79923 on 14.12.2020.
//

#ifndef PROJEKT_ARMOR_H
#define PROJEKT_ARMOR_H
#include <iostream>
#include "Object.h"

class Armor :public Object{
protected:
    std::string m_Name;
    float m_BonusProtection;
public:
    Armor(float bonusProtection,std::string name);
    virtual float getBonusProtection();
    virtual void printInfo();
    std::string getName();
};

#endif //PROJEKT_ARMOR_H
