//
// Created by avesi on 12/24/2020.
//

#ifndef PROJEKT_TILE_H
#define PROJEKT_TILE_H
#include <iostream>
#include "Enemy.h"

class Tile {
private:
    Enemy* m_enemy;
    std::string m_EnemyClass;
    std::string m_Tile;
    int m_heroPos;
    bool m_HeroHere;
public:
    Tile(int level,std::string tile);
    std::string getString();
    ~Tile();
    Enemy* getEnemy();
    int getHeroPos();
    void setHeroPos(int a);
    void setHeroHere(bool a);
    void setEnemy(Enemy* enemy);
};



#endif //PROJEKT_TILE_H
