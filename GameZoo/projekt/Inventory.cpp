//
// Created by 79923 on 14.12.2020.
//

#include "Inventory.h"
Inventory::Inventory(){

}
void Inventory::addWeapon(Weapon* weapon){
    m_Weapon.push_back(weapon);
}
void Inventory::addArmor(Armor* armor){
    m_Armor.push_back(armor);
}
void Inventory::addPotion(Potion* potion){
    m_Potion.push_back(potion);
}
void Inventory::printInfo(){
    std::cout<< "============Armor List============\n";
for(int i = 0;i < m_Armor.size();i++){
    std::cout<< "[" << i << "]"; m_Armor[i]->printInfo();
}
    std::cout<< "============Weapon List============\n";
for(int i = 0;i < m_Weapon.size();i++){
    std::cout<< "[" << i << "]"; m_Weapon[i]->printInfo();
    }
    std::cout<< "============Potion List============\n";
for(int i = 0;i < m_Potion.size();i++){
        std::cout<< "[" << i << "]"; m_Potion[i]->printInfo();
    }
}
Armor* Inventory::wearArmor(int a){
    Armor* arm = m_Armor[a];
    m_Armor.erase(m_Armor.begin()+a);
    std::cout << "You weared " << arm->getName() << std::endl;
    return arm;
};
Weapon* Inventory::wearWeapon(int a){
    Weapon* w = m_Weapon[a];
    m_Weapon.erase(m_Weapon.begin()+a);
    std::cout << "You weared " << w->getName() << std::endl;
    return w;
};
Potion* Inventory::usePotion(int a){
    Potion* p = m_Potion[a];
    m_Potion.erase(m_Potion.begin()+a);
    return p;
};
