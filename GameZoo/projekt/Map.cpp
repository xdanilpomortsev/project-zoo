//
// Created by 79923 on 14.12.2020.
//

#include "Map.h"
Map::Map(int sizeOne, int sizeTwo){
    m_Hero = new Hero("Alexey Navalny", 100, 40,  30);
createMap(sizeOne, sizeTwo);
}
void Map::createMap(int sizeOne, int sizeTwo){
    int a = 0;
    int b = 0;
    for (int i = 0; i < sizeOne; i++)
    {//Making  slots .
        m_map.push_back(std::vector<Tile*>());
    }
    for(int i = 0; i < sizeOne;i++){ // Making array of Tiles.
        for(int o = 0; o < sizeTwo; o++){
            a = rand() % 4 + 0;
            b = rand() % 2 + 0;
            if(b==0)
                m_map[i].push_back(new Tile(a,"forest"));
            else
                m_map[i].push_back(new Tile(a,"sea"));
        }
    }
    setTile(0,0);
    m_Tile->setHeroHere(true);
    m_Tile->setHeroPos(0);
}
void Map::setTile(int a, int b){
m_Tile = m_map[a][b];
}
void Map::TileDown(){
        int a = m_TilePos[0]+1, b = m_TilePos[1];
        m_Tile = m_map[a][b];
        m_TilePos[0] = a;m_TilePos[1] = b;
}
void Map::TileUp() {
    int a = m_TilePos[0]-1, b = m_TilePos[1];
    m_Tile = m_map[a][b];
    m_TilePos[0] = a;m_TilePos[1] = b;
}
void Map::TileLeft(){
    int a = m_TilePos[0], b = m_TilePos[1]-1;
    if(m_TilePos[1] > 0){
        m_Tile = m_map[a][b];
        m_TilePos[1] = b;
    }
}
void Map::TileRight(){
    int a = m_TilePos[0], b = m_TilePos[1];
    if(b < m_map[0].size()-1){
        m_Tile = m_map[a][b+1];
        m_TilePos[1] = b+1;
    }
    else
        std::cout << "wtf\n";
}
void Map::printMap() {
    m_CurrentEnemy = m_Tile->getEnemy();
    if(getEnemy()->getHelth()<1){
        m_Tile->setEnemy(nullptr);
        m_Hero->takeObject(m_CurrentEnemy);}
    for (int i = 0; i < m_map.size(); i++){
        for (int g = 0; g < m_map[i].size();g++)
            std::cout << m_map[i][g]->getString();
        std::cout << std::endl;}
}
void Map::goUp(){
    int pos = m_Tile->getHeroPos();
    if(m_TilePos[0] != 0){
        m_Tile->setHeroHere(false);
        TileUp();
        m_Tile->setHeroPos(pos);
        m_Tile->setHeroHere(true);}
    else{
        std::cout << "You can't go there\n";}
    printMap();
}
void Map::goDown(){
    int pos = m_Tile->getHeroPos();
    if(m_TilePos[0] != m_map.size()-1){
        m_Tile->setHeroHere(false);
        TileDown();
        m_Tile->setHeroPos(pos);
        m_Tile->setHeroHere(true);}
    else{
        std::cout << "You can't go there\n";}
    printMap();
}
void Map::goLeft() {
    if (m_Tile->getHeroPos() != 0)
        m_Tile->setHeroPos(m_Tile->getHeroPos() - 1);
    else {
        if(m_TilePos[1] > 0){
            m_Tile->setHeroHere(false);
            TileLeft();
            m_Tile->setHeroHere(true);
            m_Tile->setHeroPos(5);}
        else
            std::cout << "You can't go there\n";
    }
    printMap();
}
void Map::goRight() {
    if (m_Tile->getHeroPos() < 5)
        m_Tile->setHeroPos(m_Tile->getHeroPos() + 1);
    else {
        if(m_TilePos[1] < m_map[0].size()-1){
            int pos = m_TilePos[1]+1;
            m_Tile->setHeroHere(false);
            TileRight();
            m_Tile->setHeroHere(true);
            m_Tile->setHeroPos(0);}
        else
            std::cout << "You can't go there\n";
    }
    printMap();
}
Hero* Map::getHero(){
    return m_Hero;
}
Enemy* Map::getEnemy(){
    return m_CurrentEnemy;
}

