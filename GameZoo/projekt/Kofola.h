//
// Created by avesi on 12/28/2020.
//

#ifndef PROJEKT_KOFOLA_H
#define PROJEKT_KOFOLA_H
#include "Potion.h"

class Kofola :public Potion{
private:
    float m_BonusAtack;
public:
    Kofola();
    void printInfo();
    float getAtack();
};


#endif //PROJEKT_KOFOLA_H
