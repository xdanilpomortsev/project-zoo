//
// Created by 79923 on 14.12.2020.
//

#ifndef PROJEKT_CONTROL_H
#define PROJEKT_CONTROL_H
#include "Map.h"
#include "Enemy.h"
#include "GoldArmor.h"
#include "PlatinumArmor.h"
#include <string>

class Control {
public:
    Control();
    void start();
};

#endif //PROJEKT_CONTROL_H
