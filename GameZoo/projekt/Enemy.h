//
// Created by 79923 on 14.12.2020.
//

#ifndef PROJEKT_ENEMY_H
#define PROJEKT_ENEMY_H
#include <iostream>
#include "PlatinumArmor.h"
#include "GoldArmor.h"
#include "Water.h"
#include "Kofola.h"
#include "Kozel.h"
#include "Weapon.h"
#include <cstdlib>
enum class Profession{
    Soldier, Creature, Boss
};


class Enemy {
    std::string m_name;
    float m_attack;
    float m_health;
    float m_damageResist;
    Profession m_profession;
    Object* m_Object;
public:
    Enemy(std::string name, float attack, float health, float damageResist, Profession profession);
    std::string getDescriptionProfession();
    Profession getProf();
    void printInfo();
    static Enemy* createEnemy (Profession profession);
    float getHelth();
    float getAtack();
    float getResist();
    void setHelth(float a);
    Object* getObject();
    };


#endif //PROJEKT_ENEMY_H
