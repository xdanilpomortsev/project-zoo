//
// Created by avesi on 12/28/2020.
//

#include "Kozel.h"
Kozel::Kozel():Potion(){
    m_BonusArmor = 10;
    m_Type = "kozel";
}
void Kozel::printInfo(){
    std::cout << "============Object============" << std::endl;
    std::cout << "Type: " << m_Type << std::endl;
    std::cout << "Bonus health: " << m_bonusHealth << std::endl;
    std::cout << "Bonus armor: " << m_BonusArmor << std::endl;
}
float Kozel::getArmor(){
    return m_BonusArmor;
}