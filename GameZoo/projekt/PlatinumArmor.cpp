//
// Created by 79923 on 15.12.2020.
//
#include "PlatinumArmor.h"
#include <iostream>

PlatinumArmor::PlatinumArmor(float bonusProtection, float bonusAtack, std::string name):Armor(bonusProtection, name){
    m_bonusAtack = bonusAtack;
    m_Type = "platinumarmor";
}
float PlatinumArmor::getBonusProtection(){
    return m_BonusProtection;
}
float PlatinumArmor::getBonusAtack() {
    return m_bonusAtack;
}
void PlatinumArmor::printInfo(){
    std::cout << "============Object============" << std::endl;
    std::cout << "Type: PlatinumArmor" << std::endl;
    std::cout << "Bonus protection: " << m_BonusProtection << std::endl;
    std::cout << "Bonus atack: " << m_bonusAtack << std::endl;
}
