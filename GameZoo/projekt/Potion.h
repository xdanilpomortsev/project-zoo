//
// Created by 79923 on 14.12.2020.
//

#ifndef PROJEKT_POTION_H
#define PROJEKT_POTION_H
#include <iostream>
#include "Object.h"


class Potion :public Object{
protected:
    float m_bonusHealth;
public:
    Potion();
    float getBonusHealth();
    void virtual printInfo()=0;
};


#endif //PROJEKT_POTION_H
