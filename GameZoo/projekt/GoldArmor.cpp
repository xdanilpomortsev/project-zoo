//
// Created by 79923 on 15.12.2020.
//

#include "GoldArmor.h"
#include <iostream>

GoldArmor::GoldArmor(float bonusProtection, float bonusHealth, std::string name):Armor(bonusProtection,name){
  m_BonusProtection = bonusProtection;
  m_bonusHealth = bonusHealth;
  m_Type = "goldarmor";
}
float GoldArmor::getBonusProtection() {
    return m_BonusProtection;
}
float GoldArmor::getBonusHealth(){
        return m_bonusHealth;
}

void GoldArmor::printInfo(){
    std::cout << "============Object============" << std::endl;
    std::cout << "Typ: GoldArmor" << std::endl;
    std::cout << "Bonus protection: " << m_BonusProtection << std::endl;
    std::cout << "Bonus health: " << m_bonusHealth << std::endl;
}
