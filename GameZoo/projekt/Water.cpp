//
// Created by avesi on 12/28/2020.
//

#include "Water.h"
Water::Water(){
    m_bonusHealth = 40;
    m_Type = "water";
}
void Water::printInfo(){
    std::cout << "============Object============" << std::endl;
    std::cout << "Type: " << m_Type << std::endl;
    std::cout << "Bonus health: " << m_bonusHealth << std::endl;
}