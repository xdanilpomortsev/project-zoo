//
// Created by 79923 on 14.12.2020.
//

#ifndef PROJEKT_OBJECT_H
#define PROJEKT_OBJECT_H
#include <iostream>

class Object {
protected:
    std::string m_Type;
public:
    Object();
    std::string getType();
    void virtual printInfo()=0;
};


#endif //PROJEKT_OBJECT_H
