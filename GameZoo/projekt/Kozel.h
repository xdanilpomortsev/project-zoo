//
// Created by avesi on 12/28/2020.
//

#ifndef PROJEKT_KOZEL_H
#define PROJEKT_KOZEL_H
#include "Potion.h"



class Kozel : public Potion{
private:
    float m_BonusArmor;
public:
    Kozel();
    void printInfo();
    float getArmor();
};


#endif //PROJEKT_KOZEL_H
