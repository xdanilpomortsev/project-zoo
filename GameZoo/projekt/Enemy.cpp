//
// Created by 79923 on 14.12.2020.
//

#include "Enemy.h"

Enemy::Enemy(std::string name, float attack, float health, float damageResist, Profession profession){
    m_name = name;
    m_attack = attack;
    m_health = health;
    m_damageResist = damageResist;
    m_profession = profession;
    int a = rand() % 6 + 0;
    if(a==1)
        m_Object = new PlatinumArmor(10,10, "soski");
    else if(a==2)
        m_Object = new GoldArmor(10,10, "Armorino");
    else if(a==3)
        m_Object = new Water();
    else if(a==4)
        m_Object = new Kofola();
    else if(a==5)
        m_Object = new Kozel();
    else if(a==6)
        m_Object = new Weapon("Krea", 10);
}
void Enemy::printInfo(){
    std::cout << getDescriptionProfession() << "\n" ;
    std::cout << " " << m_name << "\n";
    std::cout << "Attack: \t" << m_attack << "\n" ;
    std::cout << "Health: \t" << m_health << "\n" ;
    std::cout << "DamageResist: \t" << m_damageResist << "\n\n" ;
}
std::string Enemy::getDescriptionProfession(){
        if (m_profession == Profession::Soldier){
            return "Soldier";
        } else if (m_profession == Profession::Creature){
            return "Creature";
        } else if (m_profession == Profession::Boss){
            return "Boss";
        } else {
            return "Unknown profession";
        }
}
Enemy* Enemy::createEnemy (Profession profession){
    Enemy*newEnemy = nullptr;

    if (profession == Profession::Soldier){
        newEnemy = new Enemy ("Ivan",35, 25, 5, profession);
    } else if (profession == Profession::Creature){
        newEnemy = new Enemy ("MadDog",45, 35, 10, profession);
    } else if (profession == Profession::Boss){
        newEnemy = new Enemy ("Lukashenko", 50, 50, 15, profession);
    }

    return newEnemy;

}
Profession Enemy::getProf(){
    return m_profession;
}
float Enemy::getHelth(){
    return m_health;
}
float Enemy::getAtack(){
    return m_attack;
}
float Enemy::getResist(){
    return m_damageResist;
}
void Enemy::setHelth(float a){
    if(m_health > 0)
    m_health = a;
    else
        std::cout << "Enemy is already dead, stop it and keep moving" << std::endl;
}
Object* Enemy::getObject(){
    return m_Object;
}

