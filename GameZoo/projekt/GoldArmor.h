//
// Created by 79923 on 15.12.2020.
//

#ifndef PROJEKT_GOLDARMOR_H
#define PROJEKT_GOLDARMOR_H
#include "Armor.h"
#include <iostream>

class GoldArmor: public Armor {
private:
    float m_bonusHealth;

public:
    GoldArmor(float bonusProtection,float bonusHealth, std::string name);
    float getBonusProtection();
    float getBonusHealth();
    void printInfo();
};

#endif //PROJEKT_GOLDARMOR_H
