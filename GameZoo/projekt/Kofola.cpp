//
// Created by avesi on 12/28/2020.
//

#include "Kofola.h"
Kofola::Kofola():Potion(){
    m_BonusAtack = 10;
    m_Type = "kofola";
}
void Kofola::printInfo(){
    std::cout << "============Object============" << std::endl;
    std::cout << "Type: " << m_Type << std::endl;
    std::cout << "Bonus health: " << m_bonusHealth << std::endl;
    std::cout << "Bonus atack: " << m_BonusAtack << std::endl;
}
float Kofola::getAtack(){
    return m_BonusAtack;
}