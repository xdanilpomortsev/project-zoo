//
// Created by 79923 on 14.12.2020.
//

#include "Armor.h"

Armor::Armor(float bonusProtection, std::string name){
 m_BonusProtection = bonusProtection;
 m_Type = "armor";
 m_Name = name;
}
float Armor::getBonusProtection(){
    return m_BonusProtection;
}
void Armor::printInfo(){
    std::cout << "============Object============" << std::endl;
    std::cout << "Type: " << m_Type << std::endl;
    std::cout << "Name: " << m_Name << std::endl;
    std::cout << "Bonus protection: " << m_BonusProtection << std::endl;
}
std::string Armor::getName(){
    return m_Name;
}

