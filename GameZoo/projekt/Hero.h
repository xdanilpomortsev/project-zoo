//
// Created by 79923 on 14.12.2020.
//

#ifndef PROJEKT_HERO_H
#define PROJEKT_HERO_H
#include <iostream>
#include "Inventory.h"
#include "Tile.h"
#include "Kofola.h"
#include "Kozel.h"

class Hero {
private:
    std::string m_name;
    float m_health;
    float m_atack;
    Weapon* m_weapon;
    Armor* m_armor;
    float m_protection;
    Inventory* m_inventory;
public:
    Hero(std::string name, float health, float atack, float protection);
    float getAtack();
    float getHelth();
    float getProtection();
    void setArmor(Armor* armor);
    void setWeapon(Weapon* weapon);
    void usePotion(Potion* potion);
    std::string getName();
    void atack(Enemy* enemy);
    void setHealth(float health);
    void takeObject(Enemy* enemy);
    Inventory* getInventory();
    Weapon* getWeapon();
    Armor* getArmor();
    void printInfo();
};

#endif //PROJEKT_HERO_H
