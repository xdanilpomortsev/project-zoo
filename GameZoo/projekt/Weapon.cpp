//
// Created by 79923 on 14.12.2020.
//

#include "Weapon.h"
Weapon::Weapon(std::string name, float atack ){
    m_Type = "weapon";
    m_Name = name;
    m_Atack = atack;
}
std::string Weapon::getName(){
    return m_Name;
}
float Weapon::getAtack(){
    return m_Atack;
}
void Weapon::printInfo(){
    std::cout << "============Object============" << std::endl;
    std::cout << "Type : " << m_Type << std::endl;
    std::cout << "Name: " << m_Name << std::endl;
    std::cout << "Bonus atack: " << m_Atack << std::endl;
}