//
// Created by 79923 on 14.12.2020.
//

#ifndef PROJEKT_INVENTORY_H
#define PROJEKT_INVENTORY_H
#include "Potion.h"
#include "Armor.h"
#include "Weapon.h"
#include <vector>

class Inventory {
private:
    std::vector<Armor*> m_Armor;
    std::vector<Potion*> m_Potion;
    std::vector<Weapon*> m_Weapon;
public:
    Inventory();
    void addWeapon(Weapon* weapon);
    void addArmor(Armor* armor);
    void addPotion(Potion* potion);
    void printInfo();
    Armor* wearArmor(int a);
    Weapon* wearWeapon(int a);
    Potion* usePotion(int a);
};


#endif //PROJEKT_INVENTORY_H
