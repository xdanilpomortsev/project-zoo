//
// Created by 79923 on 15.12.2020.
//

#ifndef PROJEKT_PLATINUMARMOR_H
#define PROJEKT_PLATINUMARMOR_H
#include <iostream>
#include "Armor.h"

class PlatinumArmor: public Armor {
private:
    float m_bonusAtack;
public:
    PlatinumArmor(float bonusProtection, float bonusAtack, std::string name);
    float getBonusProtection();
    float getBonusAtack();
    void printInfo();
};

#endif //PROJEKT_PLATINUMARMOR_H
