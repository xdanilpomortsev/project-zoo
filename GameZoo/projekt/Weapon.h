//
// Created by 79923 on 14.12.2020.
//

#ifndef PROJEKT_WEAPON_H
#define PROJEKT_WEAPON_H
#include <iostream>
#include "Object.h"
class Weapon :public Object{
private:
    std::string m_Name;
    float m_Atack;
public:
    Weapon(std::string name, float atack );
    std::string getName();
    float getAtack();
    void printInfo();
};


#endif //PROJEKT_WEAPON_H
