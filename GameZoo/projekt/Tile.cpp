//
// Created by avesi on 12/24/2020.
//

#include "Tile.h"
Tile::Tile(int level, std::string tile){
    if(tile=="forest")
        m_Tile = "|";
    if(tile=="sea")
        m_Tile = "~";
    if(level==0) {
        m_enemy = nullptr;
    }
    else if(level==1) {
        m_enemy = Enemy::createEnemy(Profession::Soldier);
        m_EnemyClass = "S";
    }
    else if(level==2) {
        m_enemy = Enemy::createEnemy(Profession::Creature);
        m_EnemyClass = "C";
    }
    else if(level==3) {
        m_enemy = Enemy::createEnemy(Profession::Boss);
        m_EnemyClass = "B";
    }
    m_HeroHere = false;
}
Enemy* Tile::getEnemy(){
    return m_enemy;
}
Tile::~Tile(){
    delete m_enemy;
}
std::string Tile::getString() {
    std::string a;
    a = m_Tile + m_Tile;
    if(m_enemy!= nullptr && m_enemy->getHelth()>0)
        a = a + m_EnemyClass + m_Tile;
    else
        a = a + m_Tile + m_Tile;
    a = a + m_Tile + m_Tile;
    if(m_HeroHere)
    a[m_heroPos] = 'H';
    return a;
}
int Tile::getHeroPos(){
    return m_heroPos;
}
void Tile::setHeroPos(int a){
    m_heroPos = a;
}
void Tile::setHeroHere(bool a){
    m_HeroHere = a;
}
void Tile::setEnemy(Enemy *enemy) {
    m_enemy = enemy;
}